package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func timeHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		fmt.Fprintf(w, "%s", time.Now().Format("15h04"))
	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		for key, value := range req.PostForm {
			fmt.Println(key, "=>", value)
		}
		fmt.Fprintf(w, "Information received: %v\n", req.PostForm)
	}
}

type authorEntryStruct struct {
	author string
	entry  string
}

func addHandler(res http.ResponseWriter, req *http.Request) {

	if err := req.ParseForm(); err != nil {
		fmt.Println("Something went bad")
		fmt.Fprintln(res, "Something went bad")
		return
	}

	authorEntry := authorEntryStruct{
		author: req.Form.Get("author"),
		entry:  req.Form.Get("entry"),
	}

	f, err := os.OpenFile("entries.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	if _, err := f.Write([]byte(authorEntry.author + ":" + authorEntry.entry + "\n")); err != nil {
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(res, authorEntry.author+":"+authorEntry.entry)
}

func entriesHandler(res http.ResponseWriter, req *http.Request) {
	filer, err := os.Open("entries.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer filer.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(filer)

	fmt.Fprintf(res, "%v", buf.String())
}

func main() {
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":4567", nil)
}
